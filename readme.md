=== web libraries

A set of 3rd party components for use in web clients. 
These are by various authors and are just gathered here for installation convenience.
 
* [bootstrap](http://twitter.github.com/bootstrap/) version 2.0.3
* [xsltforms](http://www.agencexml.com/xsltforms) version rc1
* [CodeMirror2-xquery](https://github.com/mbrevoort/CodeMirror2-xquery)
* [svgedit]()
* [ace](https://github.com/ajaxorg/ace-builds)

## CodeMirror2-xquery


